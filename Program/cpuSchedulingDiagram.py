import copy
#-------------------------------------------------------------------------
#リストにはプロセス番号順に入れること
#-------------------------------------------------------------------------

#[ [0]優先度　,　[1]合計処理時間　,　[2]何秒でIOに入るか？,[3]IO時間]


#dataを指定
data =[[0,12,2,2],[1,8,2,5],[1,11,5,3],[2,6,2,2],[2,8,2,1]]
#Core数を指定
AmountCore = 2
timeslice =1





#---------------------------------------------------------------------
totaldata =[]#処理用のデータ
timeList= []#合計時間のリスト
endTimeList= []#処理の終了時間のリスト
Time = 0
waittime = 0

#---------------------------------------------------------------------

END = False

SameTime = False #2コア以上の場合、Caluが二度呼ばれるので、二度目かどうかを判定する
ioList = []
priorityCounter = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]
priorityCounterR =[]
listNumber =[]
#-------------------------------------------------------------------
def setUP(d):
    c = 1
    for i in d:
        
        totaldata.append(i+[True,False,0   ,0,0,False,c,0])# [ [4]実行中?, IO時間か？, [6]連続実行時間（タイムスライス用）, [7]現在の処理時間 ,[8]IO時間カウント [9]　IOから復帰ごか？,[10]プロセス番号 ]
        timeList.append(i[1])
        priorityCounter[i[0]]= priorityCounter[i[0]] + 1
        c+=1
    view = ["" for i in d]
    #print(priorityCounter)
    priorityCounterR = priorityCounter +[0]
    return priorityCounterR,view

#結果の出力
def Answer():
    pp = ["プロセス"+str(listNumber[i]) +": "+str(endTimeList[i]) for i in range(len(listNumber))]
    print(["それぞれの終了時刻"]+ pp)
    print("全プロセス終了時刻"+str(Time))
    print()
    print("#CPU使用率" + str(sum(timeList) / Time *100) )
    print("スループット"+ str(len(timeList) / Time)) 
    print("平均ターンアラウンド時間" + str(sum(endTimeList)/len(timeList))) 
    print("平均待ち時間"+ str(waittime / len(timeList))) 
    

def Calu(tl,io,pc,v,w):
    if len(endTimeList) == len(timeList):
        return True
    UseCoreCount = 0

    tlSort =  sorted(tl, key=lambda x: x[10])
    
    #print("totaldata" +str(tl))
   
    if io!= None:
        for j in io:
            j[8] +=1#IO時間の更新
            v[j[10]-1] +="IO" #タイムチャート更新
        
    #-------------------------------------
    
    #-------------------------------------
    
    exeIndex = []#今回実行したもの
    
    for i in range(len(tl)-1):

        if AmountCore == UseCoreCount: #残りのコアがなければ処理ができないのでbreak
            break

        if tl[i][0] < tl[i+1][0]: #優先度（小さいほうが優先）
            exeIndex.append(i)
            UseCoreCount+=1
            continue  
        
        if (AmountCore-UseCoreCount) >= pc[tl[i][0]]: #コアの数（足りるなら問答無用で実行）
            pc[tl[i][0]]-=1 #優先度個数カウンターを減らす
            exeIndex.append(i)
            UseCoreCount+=1
            continue

        if tl[i][6] < timeslice:#タイムスライス()
            exeIndex.append(i)
            UseCoreCount+=1
            continue
        
        if tl[i][6] <= tl[i+1][6]:  #タイムスライスというか、連続実行時間（小さいほうが優先、同じなら先に実行なったほう（インデックスが若いほう））
            exeIndex.append(i)
            UseCoreCount+=1
            continue
        

    

    if  UseCoreCount < AmountCore:  
        exeIndex.append(len(tl)-1)
        UseCoreCount+=1

    #今回実行されなかったプロセスは連続実行時間をリセット
    
    if timeslice == 0: #タイムスライスがゼロ時はプロセス分処理時間を割る
        AC = copy.deepcopy(AmountCore)
        pList =[]
        for i in exeIndex:
            if tl[i][0] in pList:#すでに計算された優先度ならパス
                continue
            if AC >= priorityCounterR[tl[i][0]]:#同じ優先度のプロセスうよりコアのほうがおおければ
                for j in range(len(tl)):
                    if tl[j][0] == i:#同じ優先度のものは問答無用で処理時間＋1
                        AC-=priorityCounterR[tl[i][0]]
                        pList.append(tl[i][0])
                        tl[i][6]+= 1
                        tl[i][7]+= 1 
                        tl[i][9] = False
                        #print(str(i) + " == " +str(tl[j][0]))
                        v[tl[i][10]-1] +="実"

            else: 
                for j in range(len(tl)):
                    if tl[j][0] == i:
                        instanse = int(tl[i][7]) 
                        tl[i][6]+= 1/ priorityCounterR[tl[i][0]]
                        tl[i][7]+= 1/ priorityCounterR[tl[i][0]] 
                        print("tl["+str(i)+"][7]" + str(tl[i][7]))
                        tl[i][9] = False
                        if int(tl[i][7]) - instanse == 1:
                            v[tl[i][10]-1] +="実"
                        continue
    else:
        for i in range(len(tl)):
            if i in exeIndex:
    
                tl[i][6]+=1  #連続実行時間の更新
                tl[i][7]+=1  #実行時間の更新
                tl[i][9] = False
                v[tl[i][10]-1] +="実" #タイムチャート更新
                continue
            else:
                tl[i][6] = 0
                v[tl[i][10]-1] +="  " #タイムチャート更新
                tl[i][11]+=1 #待ち時間更新
    
    
    #IOに突入
    delC = 0
    for i in range(len(tl)):
        
        if tl[delC][3] !=0 and tl[delC][2] !=0 and (tl[delC][7]%tl[delC][2]) == 0  and tl[delC][7] !=0 and tl[delC][9] == False:
            tl[delC][6] =0 
            tl[delC][9] = True
            io.append(tl[delC])
            #print("iolist" +str( io))
            del tl[delC]   
            delC-=1
        delC+=1
    #-------------------------------------
  
    #-------------------------------------
    if len(io) > 0:
        #（同時にIOが終わった場合、プロセス番号が若いほうが選ばれる）
        ioc = 0
        sameTimeIoEnd = []
        for i in range(len(io)):
            #print(ioc)
            #print(io+["io"])

            if io[ioc][8] >= io[ioc][3]:#IO終了
                
                io[ioc][8] = 0          
                sameTimeIoEnd.append(io[ioc])         
                del io[ioc]
                ioc-=1
            ioc+=1

        sameTimeIoEnd= sorted(sameTimeIoEnd, key=lambda x: x[10])#同時に終わったものはプロセス番号順
        tl= sorted(tl+sameTimeIoEnd, key=lambda x: x[0])#優先度順ソート
    #-------------------------------------
    #プロセスの終了、配列からの削除
    delC = 0
    for i in range(len(tl)):
        if tl[delC][7] >= tl[delC][1]:
            endTimeList.append(Time)
            listNumber.append(tl[delC][10])
            w +=tl[delC][11]
            del tl[delC]
            delC-=1
        delC+=1
    #-------------------------------------
    pc =  copy.deepcopy(priorityCounterR)

 
    if len(endTimeList) >= len(timeList):
        return True,tl,pc,v,w
    else:
        return False,tl,pc,v,w
    

    




#-------------------------------------------------------------------
view =[]
priorityCounterR, view= setUP(data)

while END == False:
    SameTime = False
    Time= Time + 1
    END,totaldata,priorityCounter,view,waittime = Calu(totaldata,ioList,priorityCounter,view,waittime)
    #print(str(totaldata) + "totaldata")
    #print(str(Time) + "Time")
    #print(str(priorityCounter) + "priorityCounter")
    print(endTimeList)
    
    SameTime = True
    SameTime = False

print()
print()
print("「スケジューリング図」")
num = " "
for i in range(1,100):
    if i<10:
        num +=" " + str(i)
    else:
        num+= str(i)

print(num)

for i in range(len(view)):
    print(str(i+1) + view[i])

print()
print()
Answer()







