class nQueen:
    
    def __init__(self,resolve,n) :
        self.lenght = n
        self.resolve = resolve
        self.positionList = [[0 for i in range(n)] for j in range(n)]

    #縦横斜めにクイーンがないかを検索-----------------------------------------------
    def Check(self ,xy):
             #　 左　　　右　　 下　　上　　右上　  右下　 左上　  左下
        dis = [[-1,0],[1,0],[0,-1],[0,1],[1,1],[1,-1],[-1,1],[-1,-1]]
        for i in dis:
            for cc in range(1,self.lenght):
               nowX =  xy[0] + i[0] * cc
               nowY =  xy[1] + i[1] * cc
               if nowX >= self.lenght or nowX < 0 or nowY >= self.lenght or nowY < 0:
                    break
               if self.positionList[nowY][nowX] == 1:
                   return False
            
        return True
    
     #呼び出す関数
    def CheckDirectionQueen(self):
        for y in range(self.lenght):
            for x in range(self.lenght):
                #クイーンが置いてあれば検索
                if self.positionList[y][x] == 1:
                    
                    if self.Check([x,y]) == False:
                        return False            
        return True  
    #---------------------------------------------------------------------------

    #
    def nQueen(self,n,qq,kk):
        #n個のクイーンを配置したらチェスを表示
        if qq == n:
            if self.CheckDirectionQueen():
                self.resolve+=1
                print("答えパターン" + str(self.resolve))
                
                for i in self.positionList:
                    print(" ".join(["Q" * j + "*" * abs(j -1) for j in i])) 
                 
                print()
            
            return
        
        

        for l in range(kk ,n*n):
            y = int(l / n) 
            x = l % n 
            self.positionList[y][x] = 1

            if(self.CheckDirectionQueen()):
                self.nQueen(n, qq + 1, l + 1)
            
            self.positionList[y][x] = 0
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
        
#mainメソッド
if __name__ =='__main__':

    #クイーンの数を指定（ここ以外基本触らない）
    n = 8

    nQ = nQueen(0,n)
    nQ.nQueen(n,0,0)
    
